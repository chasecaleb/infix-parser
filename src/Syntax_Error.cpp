#include <sstream>
#include "Syntax_Error.h"

Syntax_Error::Syntax_Error(const std::string &m, std::stringstream &exp) {
    std::stringstream ss;

    char c;
    exp.clear();  // unget()/get()/tellg()/etc will fail if EOF was set.
    exp.unget();
    exp.get(c);

    const int pos = static_cast<int>(exp.tellg()) - 1;
    ss << m << " @ char " << pos << " [" << c << "]";
    msg = ss.str();
}

const char *Syntax_Error::what() const throw() {
    return msg.c_str();
}
