#include <iostream>
#include <cctype>
#include "InfixParser.h"
#include "Syntax_Error.h"
#include <cmath>

InfixParser::InfixParser() {
    answerSet = false;
}

InfixParser::InfixParser(std::string s) {
    set(s);
    answerSet = false;
}

void InfixParser::set(std::string s) {
    expression.clear();
    expression.str(s);
}

int InfixParser::evaluate() {
    if (expression.str().size() == 0) {
        // Setting str and seekg make error display properly.
        expression.str(" ");
        expression.seekg(1);
        throw Syntax_Error("Can't evaluate empty expression", expression);
    }
    if (answerSet) return answer;

    Token t = {true, "", 0};  // Placeholder for start of expression.
    while (expression.good()) {  // Parse entire expression.
        t = nextToken(t);
        if (t.isOperator) {
            process_operator(t);
        } else {
            values.push(t);
        }
    }

    // Continue evaluating until result obtained.
    while (!operators.empty()) {
	    if (values.size() < 2 && operators.top().s == "(") throw 
	        Syntax_Error("Missing closing parenthesis", expression);
	    evaluateOperator();
	}

	if (values.empty()) throw 
	    Syntax_Error("Value stack should contain result after parsing, but it is empty", expression);
	if (values.size() > 1) throw Syntax_Error("More than one result on stack after parsing", expression);
	answer = values.top().i;
	values.pop();
	answerSet = true;
	return answer;
}

Token InfixParser::nextToken(const Token &previous) {
    Token t;
    char c;
    expression.get(c);
    if (c == ' ') return nextToken(previous);

    if (isdigit(c)) {  // Value
        expression.putback(c);
        t.isOperator = false;
        expression >> t.i;
    } else {
        t.isOperator = true;
        char next = expression.peek();

        switch (c) {  // Operator.
            case '!':
                if (next == '=') {
                    t.s = "!=";
                    expression.ignore(1);  // Already used next char.
                } else {
                    t.s = "!";
                }
                break;
            case '+':  // + or ++
                if (next == '+') {
                    t.s = "++";
                    expression.ignore(1);
                } else {
                    t.s = "+";
                }
                break;
            case '-':  // -, --, or negative
                if (next == '-') {
                    t.s = "--";
                    expression.ignore(1);
                } else {
                    // - is binary if it follows an operand (value) or right
                    // parenthsis, unary if at beginning of input or after
                    // another operator or left parenthesis.
				    if (!previous.isOperator || previous.s == ")") {
				        t.s = "-";
				    } else {
				        t.s = "#";
				    }
				}
                break;
            case '>':  // > or >=
                if (next == '=') {
                    t.s = ">=";
                    expression.ignore(1);
                } else {
                    t.s = ">";
                }
                break;
            case '<':  // < or <=
                if (next == '=') {
                    t.s = "<=";
                    expression.ignore(1);
                } else {
                    t.s = '<';
                }
                break;
            case '=':
                if (next == '=') {
                    t.s = "==";
                    expression.ignore(1);
                } else {
                    throw Syntax_Error("Invalid character", expression);
                }
                break;
            case '&':
                if (next == '&') {
                    t.s = "&&";
                    expression.ignore(1);
                } else {
                    throw Syntax_Error("Invalid character", expression);
                }
                break;
            case '|':
                if (next == '|') {
                    t.s = "||";
                    expression.ignore(1);
                } else {
                    throw Syntax_Error("Invalid character", expression);
                }
                break;
            default:  // All the other operators.
                if (operatorTable.find({c}) == operatorTable.end()) {
                    throw Syntax_Error("Invalid character", expression);
                }
                t.s = c;
                break;
        }
    }

    // Error checking. Operators with precedence of 0 are parenthesis, 1-7 are
    // binary, and 8 are unary.
    auto lookup = operatorTable.find(t.s);
    if (t.isOperator && lookup->second.precedence > 0 && lookup->second.precedence < 8) {
        if (values.empty()) throw Syntax_Error("Binary operator without a preceding operand", expression);
        if (previous.isOperator) {
            auto prevLookup = operatorTable.find(previous.s);
            if (prevLookup->second.precedence > 0 && prevLookup->second.precedence < 8) {
                throw Syntax_Error("Two binary operators in a row", expression);
            } else if (prevLookup->second.precedence == 8) {
                throw Syntax_Error("Unary operand followed by binary operand", expression);
            }
        }
    } else if (!t.isOperator && !previous.isOperator) throw
        Syntax_Error("Two operands in a row", expression);

    return t;
}

void InfixParser::process_operator(Token t) {
	if (operators.empty() || t.s == "(") {
		if (t.s == ")") throw Syntax_Error("Unmatched closing parenthesis", expression);
		operators.push(t);
	} else {
		auto currentLookup = operatorTable.find(t.s);
		auto topLookup = operatorTable.find(operators.top().s);

		if (currentLookup->second.precedence > topLookup->second.precedence) {
			operators.push(t);
		} else if (t.s == "!" && values.empty()) {
		    operators.push(t);
		} else if (t.s == ")" && operators.top().s == "(") {
		    // Parentheses with nothing left in between.
			operators.pop();
		} else {
			// Pop all stacked operators with equal or higher precedence than
			// op.
			while (!operators.empty()
				    && (operators.top().s != "(")
				    && (currentLookup->second.precedence <= topLookup->second.precedence)) {
                evaluateOperator();
				if (t.s == ")") {
				    if (operators.empty() || operators.top().s != "(") {
				        throw Syntax_Error("Unmatched closing parenthesis", expression);
				    }
					operators.pop();
                    return;
				}
			    if (!operators.empty()) topLookup = operatorTable.find(operators.top().s);
			}
			operators.push(t);
		}
	}
}

void InfixParser::evaluateOperator() {
	int result;
	if (operators.top().s == "!" || operators.top().s == "++" ||
	        operators.top().s == "--" || operators.top().s == "#") {
		if (operators.top().s == "!") {
            if (values.empty()) throw
                Syntax_Error("Extraneous not operator", expression);
			const int rhs = values.top().i;
			values.pop();
			result = !rhs;
			operators.pop();
		} else if (operators.top().s == "++") {
			result = 1;
			Token newToken;
			newToken.isOperator = true;
			newToken.s = "+";
			operators.pop();
			operators.push(newToken);
		} else if (operators.top().s == "--") {
			result = -1;
			Token newToken;
			newToken.isOperator = true;
			newToken.s = "+";
			operators.pop();
			operators.push(newToken);
		} else if (operators.top().s == "#") {
            if (values.empty()) throw Syntax_Error("Extraneous negative sign", expression);

			const int rhs = values.top().i;
			values.pop();
			result = -1 * rhs;
			operators.pop();
		}

		Token T;
		T.isOperator = false;
		T.i = result;
		values.push(T);
	} else {
	    if (values.size() < 2) throw
	        Syntax_Error("Unknown error: value stack does not contain lhs and rhs", expression);
	    const int rhs = values.top().i;
	    values.pop();
		const int lhs = values.top().i;
		values.pop();

		if (operators.top().s == "+") {
			result = lhs + rhs;
		} else if (operators.top().s == "-") {
			result = lhs - rhs;
		} else if (operators.top().s == "*") {
			result = lhs * rhs;
		} else if (operators.top().s == "/") {
			if (rhs == 0) throw Syntax_Error("Division by zero", expression);
			result = lhs / rhs;
		} else if (operators.top().s == "%") {
			result = lhs % rhs;
		} else if (operators.top().s == "^") {
			result = int(pow(lhs, rhs));
		} else if (operators.top().s == ">") {
			result = lhs > rhs;
		} else if (operators.top().s == "<") {
			result = lhs < rhs;
		} else if (operators.top().s == ">=") {
			result = lhs >= rhs;
		} else if (operators.top().s == "<=") {
			result = lhs <= rhs;
		} else if (operators.top().s == "==") {
			result = lhs == rhs;
		} else if (operators.top().s == "!=") {
			result = lhs != rhs;
		} else if (operators.top().s == "&&") {
			result = lhs && rhs;
		} else if (operators.top().s == "||") {
			result = lhs || rhs;
		}

		Token S;
		S.isOperator = false;
		S.i = result;
		values.push(S);
        operators.pop();
    }
}
