#ifndef SYNTAX_ERROR_H_
#define SYNTAX_ERROR_H_
#include <stdexcept>
#include <string>

class Syntax_Error : public std::exception {
    public:
        Syntax_Error(const std::string &m, std::stringstream &exp);
        virtual const char *what() const throw();
    private:
        std::string msg;
};
#endif
