import glob
import subprocess

AddOption('--release', action='store_true', default=False, dest='build_release')
AddOption('--no-tests', action='store_true', default=False, dest='skip_tests')

env = Environment(
    CCFLAGS='-std=c++11',
    CPPPATH=['src', 'tests', 'tests/googletest/include/gtest',
             'tests/googletest/include/gtest/internal',
             'tests/googletest/include', 'tests/googletest'],
)

if GetOption('build_release'):
    env.Append(CCFLAGS=' -O2')  # Note: there's a space at the start of ' -02'.
else:
    dev_flags = ['-g', '-Werror', '-Wall', '-Wextra', '-pthread',
                 '-Wdisabled-optimization', '-Wold-style-cast', '-Wcast-qual',
                 '-Wlogical-op', '-Wredundant-decls', '-Wshadow',
                 '-Wsign-conversion', '-Wsign-promo', '-fno-inline']
    for i in dev_flags:
        env.Append(CCFLAGS=' ' + i)

# Build main
main_build = env.Program('build/main', Glob('src/*.cpp'))

if not GetOption('skip_tests'):
    # Google Test Framework
    env.Library('build/googletest', Glob('tests/googletest/src/*.cc'),
                CCFLAGS='-g -pthread')
    # Actual tests
    for i in glob.glob('tests/*_test.cpp'):
        test_obj = 'build/' + i.rstrip('.cpp')
        test_libs = ['googletest', 'pthread']
        src_files = glob.glob('src/*.cpp')
        src_files.remove('src/main.cpp')
        src_files.append(i)
        test_build = env.Program(test_obj, src_files, LIBS=test_libs, LIBPATH='build')
        test_run = env.Command(test_obj + '_output', None, test_obj)
        env.Depends(test_build, main_build)
