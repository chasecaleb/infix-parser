#ifndef _infix_parser_h
#define _infix_parser_h
#include <stack>
#include <unordered_map>
#include <string>
#include <sstream>
#include "Syntax_Error.h"

struct Token {
    bool isOperator;
    std::string s;  // For operator, used in conjunction with operatorTable
    int i;  // For value
};

struct Operator {
	int precedence;
	bool leftAssociative;
};

// Reference/lookup table.
const std::unordered_map<std::string, Operator> operatorTable{
    { "!", { 8, true} },
    { "++", { 8, true} },
    { "--", { 8, true} },
    { "#", { 8, true} },  // This represents a negative sign.
    { "^", { 7, false} },
    { "*", { 6, true} },
    { "/", { 6, true} },
    { "%", { 6, true} },
    { "+", { 5, true} },
    { "-", { 5, false} },  // This represents subtraction, not negative.
    { ">", { 4, true} },
    { ">=", { 4, true} },
    { "<", { 4, true} },
    { "<=", { 4, true} },
    { "==", { 3, true} },
    { "!=", { 3, true} },
    { "&&", { 2, true} },
    { "||", { 1, true} },
    { "(", { 0, true} },
    { ")", { 0, true} },
};

class InfixParser {
    public:
        InfixParser();
        InfixParser(std::string s);
        void set(std::string s);
        int evaluate();

	private:
        std::stringstream expression;
        std::stack<Token> operators;
        std::stack<Token> values;
        int answer;
        bool answerSet;

        /*
         * Helper functions for evaluate().
         */
        Token nextToken(const Token &previous);
        void process_operator(Token t);
		void evaluateOperator();
};

#endif
