#include <iostream>
#include <string>
#include <sstream>
#include "InfixParser.h"
#include "Syntax_Error.h"

int main() {
    std::string exp = "1+2*3";
    InfixParser p(exp);
    std::cout << "Result of \"" << exp << "\" is " << p.evaluate() << '\n';
	return 0;
}
