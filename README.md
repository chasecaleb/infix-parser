# Infix Parser
CS 303 project 2 -- infix expression parsing using stacks.

## Setup: Linux
1. Install scons using your distro's package manager or www.scons.org
2. Run `scons` from this directory.
3. Done. Aren't proper build systems great?

## Setup: Visual Studio
1. Open the `visual-studio/infix-parser.sln` file.
2. With any luck, running (Ctrl+F5) will run both unit tests and main.
3. *In case of errors about missing includes*: right-click the `unittests` project
   (within Visual Studio's solution explorer), select `properties`, select `VC++
   Directories` on the left, select/edit `Include Directories`, and add
   `src` and `tests/googletest/include`. Personally, I accomplished this via
   `$(SolutionDir)..\src` and `$(SolutionDir)..\tests\googletest\include`, but
   using the file browser popup should work just as well.
4. *If the `sources` and `unittests` projects are not both built/run with
   Ctrl+F5*: in the solution explorer, right-click the solution, select
   `properties`, select `Startup Project` on the left, choose `Multiple
   Startup Projects`, and then set `source` and `unittests` action to start and
   `googletest-framework` to none.
