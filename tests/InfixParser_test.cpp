#include "gtest/gtest.h"
#include "InfixParser.h"
#include "Syntax_Error.h"

TEST(ParserTest, ExpressionViaConstructor) {
    InfixParser p("1+1");
    EXPECT_EQ(2, p.evaluate());
}

TEST(ParserTest, ExpressionViaSetter) {
    InfixParser p;
    p.set("1+1");
    EXPECT_EQ(2, p.evaluate());
}

TEST(ParserTest, EvaluateMultipleTimes) {
    InfixParser p("1+1");
    EXPECT_EQ(2, p.evaluate());
    EXPECT_EQ(2, p.evaluate());
}

TEST(ParserTest, ExtraWhitespace) {
    InfixParser p("1*   2+3");
    EXPECT_EQ(5, p.evaluate());
}

TEST(ParserTest, ParenthesesUnchangedResult) {
    InfixParser p("1+(2*3)");
    EXPECT_EQ(7, p.evaluate());
}

TEST(ParserTest, ParenthesesModifiesOrderOfOperations) {
    InfixParser p("(1+2) * 3");
    EXPECT_EQ(9, p.evaluate());
}

TEST(ParserTest, Exponent) {
    InfixParser p("2 + 2^2 * 3");
    EXPECT_EQ(14, p.evaluate());
}

TEST(ParserTest, IntegerEqualityIsFalse) {
    InfixParser p("1 == 2");
    EXPECT_EQ(0, p.evaluate());
}

TEST(ParserTest, IntegerEqualityIsTrue) {
    InfixParser p("2 == 2");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, ArithmeticGreaterThanReturnsTrue) {
    InfixParser p("1 + 3 > 2");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, ArithmicGreaterThanEqualWithAndReturnsFalse) {
    InfixParser p("1 >= 4 && 0");
    EXPECT_EQ(0, p.evaluate());
}

TEST(ParserTest, AndOperatorReturnsTrue) {
    InfixParser p("2 && 2");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, OrOperatorReturnsTrue) {
    InfixParser p("2 || 0");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, OrOperatorReturnsFalse) {
    InfixParser p("0 || (1 - 1)");
    EXPECT_EQ(0, p.evaluate());
}

TEST(ParserTest, NegativeSignParenthesesAndExponents) {
    InfixParser p("(- 5 * (3^2))");
    EXPECT_EQ(-45, p.evaluate());
}

/*
 * This test exists as part of ensuring negatives are handled properly.
 */
TEST(ParserTest, Subtraction) {
    InfixParser p("2-1");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, NegativeAtBeginning) {
    InfixParser p("-5 + 1");
    EXPECT_EQ(-4, p.evaluate());
}

TEST(ParserTest, MultipleNegatives) {
    InfixParser p("-5 + -1");
    EXPECT_EQ(-6, p.evaluate());
}

TEST(ParserTest, ParenthesesAndExponents) {
	InfixParser p("(2*1+9+(9^2))");
	EXPECT_EQ(92, p.evaluate());
}

TEST(ParserTest, OuterParentheses) {
    InfixParser p("(4<=3)");
    EXPECT_EQ(0, p.evaluate());
}

TEST(ParserTest, ParenthesesAtEnd) {
    InfixParser p("2-3*(4/2)");
    EXPECT_EQ(-4, p.evaluate());
}

TEST(ParserTest, DoubleSurroundingParentheses) {
    InfixParser p("((4==4))");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, NestedParentheses) {
    InfixParser p("(2 - 7 * (3/1) + 9)");
    EXPECT_EQ(-10, p.evaluate());
}

TEST(ParserTest, NotOperatorOnEquality) {
    InfixParser p("!(1 == 1)");
    EXPECT_EQ(0, p.evaluate());
}

TEST(ParserTest, NotOperatorOnValueOfOne) {
    InfixParser p("!1");
    EXPECT_EQ(0, p.evaluate());
}

TEST(ParserTest, NotOperatorOnValueOfTwo) {
    InfixParser p("!2");
    EXPECT_EQ(0, p.evaluate());
}

TEST(ParserTest, ThreeNotOperators) {
    // !!!3 = false = 0
    InfixParser p("!!!3+2");
    EXPECT_EQ(2, p.evaluate());
}

TEST(ParserTest, ConsecutiveNotOperatorsOnBoolean) {
    InfixParser p("!!(1==1)");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, ConsecutiveNotOperatorsOnInteger) {
    // !!2 should be equivalent to Python's bool(2).
    InfixParser p("!!2");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, IncrementOperator) {
    InfixParser p("++++2-5*(3^2)");
    EXPECT_EQ(-41, p.evaluate());
}

TEST(ParserTest, DecrementOperator) {
    InfixParser p("3 + --2");
    EXPECT_EQ(4, p.evaluate());
}

TEST(ParserTest, ModOperator) {
    InfixParser p("12 % 5");
    EXPECT_EQ(2, p.evaluate());
}

TEST(ParserTest, LessThanOperatorReturnsTrue) {
    InfixParser p("2 < 3");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, LessThanOperatorReturnsFalse) {
    InfixParser p("3 < 2");
    EXPECT_EQ(0, p.evaluate());
}

TEST(ParserTest, NotEqualOperatorReturnsTrue) {
    InfixParser p("2 != 3");
    EXPECT_EQ(1, p.evaluate());
}

TEST(ParserTest, NotEqualOperatorReturnsFalse) {
    InfixParser p("2 != 2");
    EXPECT_EQ(0, p.evaluate());
}

TEST(ParserTest, InvalidCharacterAtEndThrowsError) {
    InfixParser p("1+a");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Invalid character @ char 2 [a]", e.what());
    }
}

TEST(ParserTest, InvalidCharacterAtStartThrowsError) {
    InfixParser p("x+1");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Invalid character @ char 0 [x]", e.what());
    }
}

TEST(ParserTest, ClosingParenthesisAtStartThrowsError) {
    InfixParser p(")3+2");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Unmatched closing parenthesis @ char 0 [)]", e.what());
    }
}

TEST(ParserTest, BinaryOperatorAtStartThrowsError) {
    InfixParser p("<3+2");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Binary operator without a preceding operand @ char 0 [<]", e.what());
    }
}

TEST(ParserTest, MultipleConsecutiveBinaryOperatorsThrowsError) {
    InfixParser p("3&&&&5");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Two binary operators in a row @ char 4 [&]", e.what());
    }
}

TEST(ParserTest, ConsecutiveValuesThrowsError) {
    InfixParser p("15+3 2");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Two operands in a row @ char 5 [2]", e.what());
    }
}

TEST(ParserTest, UnaryThenBinaryOperandThrowsError) {
    InfixParser p("10+ ++<3");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Unary operand followed by binary operand @ char 6 [<]", e.what());
    }
}

TEST(ParserTest, DivisionByZeroThrowsError) {
    InfixParser p("1/0");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Division by zero @ char 2 [0]", e.what());
    }
}

TEST(ParserTest, HalfOfLogicalAndThrowsError) {
    InfixParser p("1&2");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Invalid character @ char 1 [&]", e.what());
    }
}

TEST(ParserTest, HalfOfLogicalOrThrowsError) {
    InfixParser p("1|2");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Invalid character @ char 1 [|]", e.what());
    }
}

TEST(ParserTest, InvalidNotOperatorThrowsError) {
    InfixParser p("!");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Extraneous not operator @ char 0 [!]", e.what());
    }
}

TEST(ParserTest, ExtraNegativeSignThrowsError) {
    InfixParser p("- -1");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Extraneous negative sign @ char 2 [-]", e.what());
    }
}

TEST(ParserTest, MissingClosingParenthesisThrowsError) {
    InfixParser p("(1+1");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Missing closing parenthesis @ char 3 [1]", e.what());
    }
}

TEST(ParserTest, EmptyExpressionThrowsError) {
    InfixParser p("");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Can't evaluate empty expression @ char 0 [ ]", e.what());
    }
}

TEST(ParserTest, SingleEqualThrowsError) {
    // == is a valid operator, = is not
    InfixParser p("1=1");
    try {
        p.evaluate();
    } catch (std::exception &e) {
        ASSERT_STREQ("Invalid character @ char 1 [=]", e.what());
    }
}
